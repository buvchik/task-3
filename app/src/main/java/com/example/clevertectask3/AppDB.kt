package com.example.clevertectask3

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(ItemContact::class),version = 1)
abstract class AppDB:RoomDatabase(){
    abstract fun itemDAO():ItemDao
}