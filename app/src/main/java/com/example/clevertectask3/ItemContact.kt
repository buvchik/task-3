package com.example.clevertectask3

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemContact(@PrimaryKey(autoGenerate = true) var uid:Int,
                       @ColumnInfo(name = "number")var number: String,
                       @ColumnInfo(name = "name")var name: String,
                       @ColumnInfo(name = "sername")var surName: String,
                       @ColumnInfo(name = "email")var email: String)