package com.example.clevertectask3

import android.annotation.SuppressLint
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import kotlinx.android.synthetic.main.fragment_all_contacts_list.*
import kotlinx.android.synthetic.main.fragment_all_contacts_list.view.*
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class AllContactsFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {

    var listContact = mutableListOf<ItemContact>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_contacts_list, container, false)
        loaderManager.initLoader(0, null, this)

        view.listRecyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        return CursorLoader(
            activity?.applicationContext!!,
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            null
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        data?.let { contactsFromCursor(it) }
        listRecyclerView.adapter = ItemRecyclerViewAdapter(listContact)
        listRecyclerView.requestLayout()
    }

    @SuppressLint("Range")
    private fun contactsFromCursor(cursor: Cursor) {
        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                val item = ItemContact(0, "", "", "", "")
                var name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                if (name != null && !name.equals("")) {
                    var splitName = name.split(" ")
                    item.name = splitName[0]
                    if (splitName.size > 1) item.surName = splitName[1]

                    val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val emails = context?.getContentResolver()?.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id,
                        null,
                        null
                    )!!
                    while (emails.moveToNext()) {
                        item.email =
                            emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                    }
                    val phones = context?.getContentResolver()?.query(
                        Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = " + id, null, null
                    )!!
                    while (phones.moveToNext()) {
                        item.number = phones.getString(phones.getColumnIndex(Phone.NUMBER))
                    }
                    listContact.add(item)
                }

            } while (cursor.moveToNext())
            saveToDB()
        }

    }

    fun saveToDB() {
        val db =
            context?.let { Room.databaseBuilder(it, AppDB::class.java, "database-app").build() }
        val itemDao = db?.itemDAO()
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                listContact.forEach {
                    itemDao?.insert(it)
                }
            }
            Toast.makeText(context, "Сохранение успешно", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        loader.reset()
    }


}

class ItemRecyclerViewAdapter(private val values: List<ItemContact>) :
    RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.name.text = item.name
        holder.number.text = item.number
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: TextView
        val number: TextView

        init {
            name = v.findViewById(R.id.tvName)
            number = v.findViewById(R.id.tvNumber)
        }

    }

}