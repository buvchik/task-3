package com.example.clevertectask3

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.fragment_main.*
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnShowContact.setOnClickListener {
            val hasPermission =
                context?.let { checkSelfPermission(it, Manifest.permission.READ_CONTACTS) }
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    context as Activity, arrayOf(Manifest.permission.READ_CONTACTS), 100
                )
            } else activity?.supportFragmentManager?.commit {
                replace(R.id.container, AllContactsFragment())
                addToBackStack("listFragment")
            }
        }
        btnSelectContact.setOnClickListener {
            createDialog()
        }
        btnShowSP.setOnClickListener {
            val sharedPref =
                activity?.getPreferences(Context.MODE_PRIVATE) ?: return@setOnClickListener
            val highScore = sharedPref.getString("number", "number empty")
            Toast.makeText(context, highScore, Toast.LENGTH_SHORT).show()

        }
    }

    var listNumber = mutableListOf<String>()
    fun createDialog() {

        lifecycleScope.launch {
            val builder: AlertDialog.Builder? = activity?.let { AlertDialog.Builder(it) }

            withContext(Dispatchers.IO) {
                val db = context?.let {
                    Room.databaseBuilder(it, AppDB::class.java, "database-app").build()
                }
                val itemDao = db?.itemDAO()
                var list = itemDao?.getAllNumber()
                list?.forEach { listNumber.add(it.number) }
                builder?.setTitle("Contacts")
                builder?.setItems(listNumber.toTypedArray(), { dialog, i ->
                    lifecycleScope.launch {
                        withContext(Dispatchers.IO) {
                            var info = itemDao?.findByNumber(listNumber.get(i)) as ItemContact
                            withContext(Dispatchers.Main) {
                                tvInfo.setTextAppearance(context, R.style.for_tv_title)
                                tvInfo.setText("Name: " + info.name + "\n" + "SurName: " + info.surName + "\n" + "Number: " + info.number + "\n" + "E-mail: " + info.email)
                            }
                            val sharedPref =
                                activity?.getPreferences(Context.MODE_PRIVATE) ?: return@withContext
                            with(sharedPref.edit()) {
                                putString("number", info.number)
                                apply()
                            }
                        }
                    }
                })
            }
            builder?.create()
            builder?.show()
        }
    }

}