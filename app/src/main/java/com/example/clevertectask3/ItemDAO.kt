package com.example.clevertectask3

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ItemDao {

    @Query("SELECT * FROM itemcontact")
    fun getAllNumber(): List<ItemContact>

    @Query("SELECT * FROM itemcontact WHERE number LIKE :first  LIMIT 1")
    fun findByNumber(first: String): ItemContact

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item:ItemContact)

}
